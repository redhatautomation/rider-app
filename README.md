#  Requirements
Due to some potential issue with `pkg-config` on Fedora (or something else in the RPMs?) the folowing were needed to get cros-compiling/static linking working with rust:
```
dnf install perl musl-gcc
export OPENSSL_DIR=/usr/include/openssl/
export OPENSSL_LIB_DIR=/usr/lib64/
export OPENSSL_INCLUDE_DIR="/usr/include/"
```

# Building
it's easy to build for target system
```
cargo build
```

However, for static linking the following is required
```
# install rustup
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# add musl
rustup target add x86_64-unknown-linux-musl

# build
cargo build --target=x86_64-unknown-linux-musl --release
```
