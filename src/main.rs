use serde_yaml;
use serde::{Serialize, Deserialize};
use serde_json::json;
use chrono::Local;
use rand::Rng;
use http::StatusCode;

use elasticsearch::{
    http::transport::Transport,
    params::Refresh, Elasticsearch, IndexParts,
    indices::{ IndicesExistsParts, IndicesCreateParts, }
};

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Location {
    lat: f64,
    lon: f64,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Config {
    es_url: String,
    hostname: String,
    es_index: String,
    battery_level: f64,
    update_interval: u64,
    location: Location,
}

// this is here to manually build index patterns, typically devices do not build them
async fn create_index_if_not_exists(client: &Elasticsearch, es_index: &String) -> Result<(), Box<dyn std::error::Error>> {
    let exists = client
        .indices()
        .exists(IndicesExistsParts::Index(&[es_index]))
        .send()
        .await?;

    if exists.status_code() == StatusCode::NOT_FOUND {
        let mapping_response = client
            .indices()
            .create(IndicesCreateParts::Index(es_index)) .body(json!({ "mappings": {
                    "properties": {
                        "hostname":  {
                            "type": "keyword",
                        },
                        "post_date": {
                            "type": "date",
                        },
                        "location": {
                            "type": "geo_point",
                        },
                        "battery-level": {
                            "type": "float",
                        },
                    }
                }
            }))
            .send()
            .await?;

        if !mapping_response.status_code().is_success() {
            panic!("mapping failed")
        }
    }
    Ok(())
}

async fn update(client: &Elasticsearch, config: &mut Config) -> Result<(), Box<dyn std::error::Error>> {
    let date = Local::now()
        .format("%Y-%m-%dT%H:%M:%SZ")
        .to_string();

    if config.battery_level > 0.02 {
        // initialize RNG
        let mut rng = rand::thread_rng();

        let diff_lat:f64 = rng.gen_range(-0.001..0.001);
        let diff_lon:f64 = rng.gen_range(-0.001..0.001);

        let distance:f64 = f64::sqrt(diff_lat.powf(2.0) + diff_lon.powf(2.0));

        // move our machine
        config.battery_level = config.battery_level - distance;
        config.location.lat = config.location.lat + diff_lat;
        config.location.lon = config.location.lon + diff_lon;
    }

    println!("sending update: {}\n\tlat: {}\n\tlon: {}\n\tbat: {}", date, config.location.lat, config.location.lon, config.battery_level);

    let index_response = client
        .index(IndexParts::Index(&config.es_index))
        .body(json!({
            "hostname": config.hostname,
            "post_date": date,
            "location": config.location,
            "battery_level": config.battery_level,
        }))
        .refresh(Refresh::WaitFor)
        .send()
        .await?;

    if !index_response.status_code().is_success() {
        println!("indexing document failed, continuing")
    }

    std::thread::sleep(std::time::Duration::from_millis(config.update_interval));
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("RiderDaemon Starting!");

    let config_f = std::fs::File::open("config.yaml").unwrap();
    let mut config: Config= serde_yaml::from_reader(config_f) 
        .unwrap();

    println!("\telastic url: {}\n\thostname: {}\n\tindex: {}", config.es_url, config.hostname, config.es_index);

    let transport = Transport::single_node(&config.es_url)?;
    let client = Elasticsearch::new(transport);
    create_index_if_not_exists(&client, &config.es_index).await?;
    loop {
        update(&client, &mut config).await?;
    }
}
